<?php

namespace Tests;

use App\Call;
use App\Mobile;
use App\Carrier;
use PHPUnit\Framework\TestCase;
use App\Services\ContactService;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$mobile = new Mobile(new Carrier("Verizon"));
		$this->assertNull($mobile->makeCallByName(''));
	}
	
	/** @test */
	public function it_returns_a_call_when_not_empty()
	{
		$mobile = new Mobile(new Carrier("AT&T"));
		$this->assertInstanceOf(Call::class, $mobile->makeCallByName("john"));
	}

	/** @test */
	public function it_returns_false_if_you_use_an_invalid_number()
	{
		$this->assertFalse(ContactService::sendSMS("invalid number xD"));
	}

	/** @test */
	public function it_returns_true_if_sends_the_message()
	{
		// Eg. +50688888888 
		// Write youre number here, it worked with me, if don't work maybe is something else
		$this->assertTrue(ContactService::sendSMS("+50688888888"));
	}

}
