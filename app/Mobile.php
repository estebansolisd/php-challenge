<?php

namespace App;

use App\Carrier;
use App\Services\ContactService;
class Mobile
{

	protected $provider;
	
	function __construct(Carrier $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $contact->name ? $this->provider->makeCall() : null;
	}

}
