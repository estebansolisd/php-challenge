<?php

namespace App;
use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;

class Carrier implements CarrierInterface
{
    private $carrier;
    function __construct($carrier)
    {
        $this->carrier = $carrier;
    }
    public function dialContact(Contact $contact): void
    {
        echo "Starting the call using the carrier: $this->carrier \n";
        echo "Calling to $contact->name \n";
        echo "Call in progress \n";
        echo "Call duration " . rand(0, 99) . ":" . rand(0, 99) . ":" . rand(0, 99) . "\n";
        echo "Call ended \n";
    }

    public function makeCall(): Call {
        return new Call();
    }
}
