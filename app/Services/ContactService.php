<?php

namespace App\Services;

use App\Contact;
use Exception;
use Twilio\Rest\Client;

class ContactService
{
	private static $contacts = [
		["name" => "john", "phone_number" => "+1 (541) 272-1234"],
		["name" => "doe", "phone_number" => "+1 (532) 272-1234"],
		["name" => "foo", "phone_number" => "+1 (521) 332-1234"]
	];

	public static function findByName(string $name): Contact
	{
		$result = array_filter(self::$contacts, function ($contact) use ($name) {
			return $contact["name"] == $name;
		});
		return count($result) <= 0 ? new Contact("", "") : new Contact($result[0]["name"], $result[0]["phone_number"]);
	}

	public static function validateNumber(string $number): bool
	{
		return preg_match("/^[\+\d]?(?:[\d\-.\s()]*)$/im", $number);
	}

	public static function sendSMS(string $number): bool
	{
		$isMessageSent = false;
		if (!self::validateNumber($number)) {
			return $isMessageSent;
		}

		echo "Sending message to phone_number -> $number";

		try{
			// Only for development propuses
			$sid = 'AC939cfac78d5a5e54ee432b12458b4ff7';
			$token = 'b92c1096e52a4d50d4827012a0376b5c';
			$client = new Client($sid, $token);
	
			// Use the client to do fun stuff like send text messages!
			$client->messages->create(
				// the number you'd like to send the message to
				$number,
				[
					'from' => '+13177933268',
					'body' => 'Hey this is the message from the challenge'
				]
			);
			$isMessageSent = true;
		}catch(Exception $ex) {
			echo "Error trying to send this message";
			$isMessageSent = false;
		}



		return $isMessageSent;
	}
}
