<?php

namespace App;


class Contact
{
	public $name;
	public $number;

	function __construct($name, $number)
	{
		$this->name = $name;
		$this->number = $number;
	}
}